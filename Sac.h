#ifndef SAC_H
#define SAC_H
#include <iostream>
#include <vector>
#include "Brique.h"
/*!
 *
 */
namespace tetris {
/*!
 * \brief The Sac class Represente virtuellement le sac du joueur contenant des briques
 */
class Sac{
private:
    /*!
     * \brief sac les formes des briques
     */
    std::vector<Brique> sac;
    /*!
     * \brief LesPieceDuJeu Contient toutes les pièces du jeu.
     */
    std::vector<Brique> LesPieceDuJeu;


public:

    /*!
     * \brief Sac Construction du sac
     */
     Sac(std::vector<Brique> vectorBrique);


    ~Sac();
     /*!
     * \brief Sac Constructeur de recopie de Sac
     * \param sacARecopier Le sac a recopier
     */
    Sac(Sac const& sacARecopier);
    Sac();
    /*!
     * \brief getForme Donne la forme de la brique
     * \return retourne la forme de la brique
     */
    std::vector<Brique> getForme();

    /*!
     * \brief prochaineBriqueCourante
     * \return
     */
    Brique prochaineBriqueCourante();

    /*!
     * \brief melangerSac Mélange le sac
     */
    void melangerSac();

    /*!
     * \brief getCourant Donne la brique courante
     * \return Retourne la brique courante
     */
    Brique getCourant();
    /*!
     * \brief addBrique ajoute la nouvelle brique dans le sac et permet de l'afficher
     * \return Vrai si ajouter,faux sinon.
     */
    bool ajouterBrique(Brique brique);
    /*!
     * \brief removeBrique Supprime la brique de la vectore de jeu et du sac
     * \param brique Brique à supprimer
     * \return Vrai si supprimer, faux sinon
     */
    bool supprimerBrique(Brique brique);
    /*!
     * \brief getAllBrique retourne toutes les pièces
     * \return  Toutes les pièces du jeu.
     */
    std::vector<Brique> getToutesBrique();
};
}
#endif // SAC_H
