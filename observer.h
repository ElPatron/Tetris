#ifndef OBSERVER_H
#define OBSERVER_H
namespace tetris {
/*!
 * \brief The Observer class est l'interface de l'observé.
 */
class Observer{
    /*!
   * \brief update met à jour les observers.
   */
  virtual void update()=0;
};
}

#endif // OBSERVER_H
