#include <iostream>
#include <list>
#include "Point.h"

namespace tetris{
Point::Point(int x,int y):thisX{x},thisY{y}{

}

/*!
 * \brief Point
 */
Point::Point(Point const& point):thisX(point.thisX),thisY(point.thisY){

}


int Point::getX(){
    return thisX;
}


int Point::getY(){
    return thisY;
}
}
