#ifndef POINT_H
#define POINT_H
#include <iostream>
namespace tetris {
/*!
 * \brief The Point class Représente un point.
 */
class Point{
protected:
    /*!
     * \brief x l'abscisse du point
     */
    int thisX;
    /*!
     * \brief y l'ordonée du point
     */
    int thisY;
public:
    /*!
     * \brief Point Construction du point
     * \param x l'abscisse du point
     * \param y l'ordonée du point
     */
    Point(int x,int y);
    ~Point();
    /*!
     * \brief Point Construteur de recopie de Point
     * \param point Point a recopier
     */
    Point(Point const& point);
    /*!
     * \brief getX Retourne l'abscisse du point
     * \return Retourne l'abscisse du point
     */
    int getX();
    /*!
     * \brief getY Retourne l'ordonnée du point
     * \return Retourne l'ordonnée du point
     */
    int getY();


};
}
#endif // POINT_H
