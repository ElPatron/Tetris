#ifndef MOVE_H
#define MOVE_H
namespace tetris {
/*!
 * \brief The Move class représente un mouvement dans le jeu
 */
class Move:public Interaction{
    /*!
      * \brief translationX Translate une pièce.
      * \param cote Le coté vers lequel translater
      * \param plateau Le plateau de jeu
      */
     void translationX(std::string cote,Plateau plateau)=0;
    /*!
     * \brief rotation Rotation de la brique.
     */
     void rotation(Plateau plateau)=0;
    /*!
     * \brief drop Laisse tomber la brique.
     */
     void drop(Plateau plateau)=0;
};

}
#endif // MOVE_H
