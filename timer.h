#ifndef TIMER_H
#define TIMER_H
#include "Plateau.h"
namespace tetris {
/*!
 * \brief The Timer class Le timer représente tout ce qui se rapporte au temps dans le jeu
 */
class Timer{
private:
    /*!
     * \brief tempsEcoule Le compte à rebours du jeu.
     */
    int tempsEcoule;
public:
    /*!
     * \brief Timer Constructeur du temps
     * \param plateau Represente le plateau de jeu
     */
    Timer(Plateau plateau);
    ~Timer();
    /*!
     * \brief Timer Constructeur de recopier du temps
     * \param tempsARecopier le temps a copier
     */
    Timer(Timer const& tempsARecopier);
};
}
#endif // TIMER_H
