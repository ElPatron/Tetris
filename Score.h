#ifndef SCORE_H
#define SCORE_H
#include <iostream>
#include <vector>
/*!
 *
 */
namespace tetris {
/*!
 * \brief The Score class Le score du jeu
 */
class Score{
private:
    /*!
     * \brief score Score de départ
     */
    int score=0;
    /*!
     * \brief maxScore le score maximum que le joueur peut atteindre
     */
    int maxScore=10000;
    /*!
     * \brief valeurConstant Valeur constant permettant le calcul du score
     */
    std::vector<int> valeurConstant;
public:
    /*!
     * \brief Score Construit le score du Joueur.
     */
    Score();

    ~Score();
    /*!
     * \brief Score Constructeur de recopie de Score
     * \param scoreARecopier score a recopier
     */
    Score(Score const& scoreARecopier);

    /*!
     * \brief translationY Calcul du score avec la translation vers le bas
     * \param nombreLigne nombre de ligne effectué
     * \param nombreCase difference entre le brique et l'endroit où il s'est posé
     */
    void translationY(int nombreLigne,int nombreCase);

    /*!
     * \brief drop Calcul du score lors du drop.
     * \param nombreLigne nombre de ligne effectué
     * \param nombreCase difference entre le brique et l'endroit où il s'est posé
     */
    void drop(int nombreLigne,int nombreCase);

    /*!
     * \brief getScore Retoune le score
     * \return Retoune le score
     */
    int getScore();

    /*!
     * \brief calculPersonnalisé Calcul lorqu'il y a plus de 4 ligne
     * \param nombreLigne nombre de ligne effectué
     * \param nombreCase difference entre le brique et l'endroit où il s'est posé
     * \return Retourne le score.
     */
    int calculPersonnalisé(int nombreLigne,int nombreCase);

    /*!
     * \brief verifScore Vérifie si le score n'a pas atteint le score maximum
     * \return Retourne si le score n'a pas atteint le score maximum
     */
    bool verifScore();

};
}
#endif // SCORE_H
