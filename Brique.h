#ifndef BRIQUE_H
#define BRIQUE_H
#include <iostream>
#include <list>
#include "Carree.h"
#include <string.h>
/*!
 * \brief Espace de nom de tétris.
 *
 */
namespace tetris {
/*!
 * \brief La brique est un élément du jeu Tétris.
 */
class Brique{
private:
    /*!
     * \brief listCarree La liste des carrées qui constitue la brique.
     */
    std::list<Carree> listCarree;
    /*!
     * \brief briquePerso Si la brique est une brique personnalisé ou pas.
     */
    bool briquePerso;


public:
    /*!
     * \brief Brique Construit la brique selon la liste de position données.
     * \param x
     * \param y
     * \param listeChaine Liste de point qui permet de personnaliser la brique
     */
    Brique(int x,int y,std::list<std::string> listeChaine);

    /*!
     * \brief Brique Construit une brique par défaut.
     * \param numero Le numéro de la brique par défaut à construire.
     */
    Brique(int numero);

    ~Brique();

    /*!
     * \brief Brique Constructeur par recopie de Brique
     * \param brique La brique à recopier
     */
    Brique(Brique const& brique);
    /*!
     * \brief getRecopieChaine retourne la liste de chaine.
     */

    std::list<Carree> getListCarre();
    /*!
     * \brief translationX Translate la pièce vers le coté souhaité
     * \param cote Si il fait aller à gauche ou à droite
     */
     void translationX(std::string cote);

    /*!
     * \brief rotation Fais une rotation de la pièce de 90° à chaque fois.
     */
     void rotation();



};
}
#endif // BRIQUE_H
