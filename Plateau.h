#ifndef PLATEAU_H
#define PLATEAU_H
#include <iostream>
#include <vector>
#include "Case.h"
#include "observer.h"
namespace tetris {
/*!
 * \brief The Plateau class représente le plateau de jeu.
 */
class Plateau{
private:
    /*!
     * \brief hauteur La hauteur du plateau.
     */
    int hauteur;
    /*!
     * \brief largeur La largeur du plateau
     */
    int largeur;
    /*!
     * \brief plateau Le plateau du jeu.
     */
    vector<vector<Case>> plateau;
    /*!
     * \brief haut le haut du plateau
     */
    int haut;
    /*!
     * \brief bas le bas du plateau
     */
    int bas;


    /*!
     * \brief joueur Le joueur courant
     */
    Joueur joueur;
    /*!
     * \brief niveau le niveau du jeu
     */
    NiveauJeu niveau;
    /*!
     * \brief compteurLigne compte les lignes effectués.
     */
    int compteurLigne;
    /*!
     * \brief LigneMax ligne maximum que doit faire le joueur
     */
    int LigneMax=25;
    /*!
     * \brief ligneOccupe verifie si la ligne est complete ou non.
     */
    bool ligneOccupe;
    /*!
     * \brief xDeplacement Deplacement de la brique ver la gauche ou droite.
     */
    int xDeplacement;
    /*!
     * \brief yDeplacement Deplacement de la brique vers le bas.
     */
    int yDeplacement;
    /*!
     * \brief listeOccupé Si le plateau est occupé ou non.
     */
    bool plateauOccupé;
    /*!
     * \brief listObserver La liste des observés.
     */
    vector<Observer> listObserver;
public:
    /*!
     * \brief Plateau Construction du plateau du jeu
     * \param hauteur la hauteur du plateau
     * \param largeur la largeur du plateau
     * \param niveau le niveau de jeu
     */
    Plateau(int hauteur,int largeur,int niveau=1);

    ~Plateau();

    /*!
     * \brief Plateau
     */
    Plateau(Plateau const&);

    /*!
     * \brief getHauteur Retourne la hauteur du plateau
     * \return Retourne la hauteur du plateau
     */
    int getHauteur();

    /*!
     * \brief getLargeur Retourne la largeur du plateau
     * \return Retourne la largeur du plateau
     */
    int getLargeur();

    /*!
     * \brief getPlateau Retourne le plateau de jeu
     * \return Retourne le plateau de jeu
     */
    std::vector<Case,Case>getPlateau();

    /*!
     * \brief getHaut Retourne le haut du plateau
     * \return Retourne le haut du plateau
     */
    int getHaut();

    /*!
     * \brief getBas Retourne le bas du plateau
     * \return Retourne le bas du plateau
     */
    int getBas();

    /*!
     * \brief verificationCaseSuivante
     * \param x l'abscisse du plateau
     * \param y l'ordonne du plateau
     * \return
     */
    bool verificationCaseSuivante(int x,int y);

    /*!
     * \brief finDuJeu Lorsque le joueur n'arrive plus a mettre de brique
     */
    void finDuJeu();
    // A modifier
    /*!
     * \brief finDuTemps Le temps du jeu est écoulé.
     * \return si le jeu s'est arreté ou non.
     */
    bool finDuTemps();

    /*!
     * \brief ajouterAuPlateau Ajout de la brique dans le plateau
     * \param point les points du plateau
     * \return Retourne le plateau modifié
     */
    std::vector<Case,Case>ajouterAuPlateauLaBrique(Point point);

    /*!
     * \brief prochaineBrique Donne la brique suivante
     */
    void prochaineBrique();

    /*!
     * \brief nouvellePartie Fait une nouvelle partie dans le jeu
     */
    void nouvellePartie();

    /*!
     * \brief rotationBrique fait une rotation de la brique
     */
    void rotationBrique();

    /*!
     * \brief translationBrique translation vers la droite ou gauche de la brique
     * \param cote
     */
    void translationBrique(std::string cote);

    /*!
     * \brief dropBrique Laisse tomber la brique
     */
    void dropBrique();

    /*!
     * \brief verifNivJeu Vérifie si le niveau de jeu n'a pas atteint le maximum ou non
     * \return si le niveau de jeu n'a pas atteint le maximum ou non
     */
    bool verifNivJeu();

    /*!
     * \brief augmenterNivJeu Augmente le niveau de jeu
     */
    void augmenterNivJeu();

    /*!
     * \brief verifScore Vérifie si le score a atteint son maximum ou non
     * \return Vérifie si le score a atteint son maximum ou non
     */
    bool verifScore();

    /*!
     * \brief majScore Mis à jour du score.
     */
    void majScore();

    /*!
     * \brief verificationLigneExistante Vérifie s'il y a une ligne existante qui est complété ou non
     * \return Vérifie s'il y a une ligne qui a été complété ou non
     */
    bool verificationLigneExistante();

    /*!
     * \brief compteLigneMax Vérifie si le joueur a effectué le nombre de ligne maximum ou non
     * \return Vérifie si le joueur a effectué le nombre de ligne maximum ou non
     */
    bool compteLigneMax();

    /*!
     * \brief rechercherJoueurEnLigne Recherche d'un joueur existant
     */
    void rechercherJoueurEnLigne();

    /*!
     * \brief avertirAdversaire Avertit l'adversaire que le joueur a posé une brique
     */
    void avertirAdversaire();

    /*!
     * \brief ajoutLigneParAdversaire L'ajout de la ligne dans le plateau adverse
     */
    void ajoutLigneParAdversaire();

    /*!
     * \brief remonterBriqueCourante Remonte la brique courante si l'adversaire vient d'ajouter des lignes
     * au plateau du joueur.
     */
    void remonterBriqueCourante();
};
}


#endif // PLATEAU_H
