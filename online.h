#ifndef ONLINE_H
#define ONLINE_H
namespace tetris {
/*!
 * \brief The Online class représente la gestion du multijoueur.
 */
class Online{
    /*!
   * \brief ajoutLigneAdversaire Ajoute des lignes à l'adversaire
   */
  bool ajoutLigneAdversaire();
  /*!
   * \brief connexionAvdersaire Si l'adversaire est en ligne
   * \return Si il est en ligne ou pas
   */
  bool connexionAvdersaire();
  /*!
   * \brief actionPourAdversaire Préviens l'adversaire pour un ajout de ligne
   * \param ligne  Nombre de ligne à ajouer
   * \return si à reussi à avertir l'adversaire
   */
  bool actionPourAdversaire(int ligne);
  /*!
   * \brief rechercheJoueur recherche d'un joueur pour débuter une partie
   * \return si trouvé ou non
   */
  bool rechercheJoueur();
};
}
#endif // ONLINE_H
