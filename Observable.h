 #ifndef OBSERVABLE_H
#define OBSERVABLE_H
namespace tetris {
/*!
 * \brief The Observable class est l'interface de l'observateur.
 */
class Observable{
    /*!
   * \brief registerObserver enregistre un nouvel observer
   * \param obs Le nouvel observer à enregistrer
   */
  virtual void registerObserver(Observer obs)=0;
    /*!
   * \brief removeObserver supprimer un observer
   * \param obs L'observer à supprimer
   */
  virtual void removeObserver(Observer obs)=0;
    /*!
   * \brief notifyObserver notifie les observers
   */
  virtual void notifyObserver()=0;
};
}
#endif // OBSERVABLE_H
