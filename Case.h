#ifndef CASE_H
#define CASE_H
#include <iostream>
#include "Point.h"
namespace tetris {
/*!
 * \brief The Case class est une réprésentation virtuel du plateau de jeu Tétris.
 */
class Case:public Point{
private:
    /*!
     * \brief occupation représente l'occupation de la case.
     */
    bool occupation;

public:
    /*!
     * \brief Case Construction de la case dans le plateau
     * \param x l'abscisse de la case du plateau
     * \param y l'ordonnée de la case du plateau
     */
    Case(int x,int y);

    ~Case();

    /*!
     * \brief Case recopie de la case
     * \param laCase la case à recopier
     */
    Case(Case const& laCase);
    /*!
     * \brief estVide Permet de savoir si la case est vide ou pas.
     * \return vrai si elle est rempli, faux sinon.
     */
    bool estRempli();
    /*!
     * \brief remplir permet de remplir la case et ainsi la rendre inutilisable.
     */
    void remplir();
    /*!
     * \brief remplirTemporairement rempli la case temporairement afin de pouvoir faire descendre la case.
     */
    bool remplirTemporairement();
    /*!
     * \brief vider Permet de remettre la case à vide afin qu'elle soit considérer comme vide.
     */
    void vider();
};
}
#endif // CASE_H
