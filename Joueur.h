#ifndef JOUEUR_H
#define JOUEUR_H
#include <iostream>
#include <string>
#include "Sac.h"
namespace tetris{
/*!
 * \brief The Joueur class représente le joueur dans le jeu
 */
class Joueur{
private:
    /*!
     * \brief nomJoueur
     */
    std::string nomJoueur;
    /*!
     * \brief sacJoueur Sac du joueur
     */
    Sac sacJoueur;
    /*!
     * \brief multiJoueur
     */
    bool multiJoueur;
public:
    /*!
     * \brief Joueur Constructeur du joueur.
     * \param nom Son nom
     * \param multi S'il est dans une parti multijoueur ou pas
     */
    Joueur(std::string nom,bool multi);

    ~Joueur();

    /*!
      * \brief Joueur constructeur par recopie de joueur
     * \param jouerACopier Joueur à copier
     */
    Joueur(Joueur const& jouerACopier);
    /*!
     * \brief getNom donne le nom du joueur
     * \return Le nom du joueur
     */
    std::string getNom();
    /*!
     * \brief getMultiJoueur Si le joueur joue en ligne ou pas.
     * \return Vrai si le joueur joue en ligne sinon Faux.
     */
    bool getMultiJoueur();
    
    /*!
     * \brief getSac retourne le sac du joueur.
     * \return Le sac du joueur.
     */
    Sac getSac();

    /*!
     * \brief ajouterPiece permet d'ajouter une pièce à ajouter au sac
     */
    void ajouterPiece(Brique &brique);
    
};
}


#endif // JOUEUR_H
