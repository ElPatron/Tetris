#include <iostream>
#include <list>
#include "Case.h"
#include "Point.h"
namespace tetris{

    Case::Case(int x,int y):Point(x,y),occupation(false){

    }


    Case::Case(Case const& laCase):Case(laCase.thisX,laCase.thisY){// se renseigner pour les getter en const

    }

    bool Case::estRempli(){
        return occupation;
    }

    void Case::remplir(){
        occupation=true;
    }

    bool Case::remplirTemporairement(){

    }

    void Case::vider(){
        occupation=false;
    }

}
