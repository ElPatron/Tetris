#ifndef INTERACTION_H
#define INTERACTION_H
#include <iostream>
namespace tetris {
/*!
 * \brief The Interaction class permet à l'utilisateur d'intéragir avec la pièce.
 */
class Interaction{
public:
    /*!
     * \brief translationX Translation ver la droite ou gauche de la brique.
     */
    virtual void translationX(std::string cote,Plateau plateau)=0;
    /*!
     * \brief rotation Rotation de la brique.
     */
    virtual void rotation(Plateau plateau)=0;
    /*!
     * \brief drop Laisse tomber la brique.
     */
    virtual void drop(Plateau plateau)=0;

};
}
#endif // INTERACTION_H
