#ifndef NIVEAUJEU_H
#define NIVEAUJEU_H
#include <iostream>
#include <vector>
#include "Plateau.h"
namespace tetris {
/*!
 * \brief The NiveauJeu class représente le niveau du jeu dans le jeu
 */
class NiveauJeu{
private:

    /*!
     * \brief niveauJeu Tableau a taille variable ou la taille
     * represente le niveau de jeu maximal et le contenu de
     * chaque case represente le temps du niveau de jeu
     */
    std::vector<double> niveauJeu;

    /*!
     * \brief niveauCourant represente le niveau courant du jeu
     * permettant d'acceder au temps du niveau de jeu
     */
    int niveauCourant;


public :

    /*!
     * \brief NiveauJeu Construction du niveau de jeu.
     */
    NiveauJeu();

    ~NiveauJeu();

    /*!
     * \brief NiveauJeu Constructeur de recopie de Niveau de Jeu
     * \param NiveauJeuACopier Niveau de jeu a recopier
     */
    NiveauJeu(NiveauJeu const & NiveauJeuACopier);

    /*!
     * \brief getNiveauJeu Retourne le niveau de jeu.
     * \return Retourne le niveau de jeu.
     */
    int getNiveauJeu();
    /*!
     * \brief augmenterNivauJeu Chaque 10 lignes on incrémente le niveau de jeu.
     * \return retourne le niveau de jeu aprés incrémentation.
     */
    void augmenterNivauJeu();

    /*!
     * \brief verifNiveauJeu verifie le niveau de jeu pour voir si le compteurLigne est à 10 ou pas.
     */
    void verifNiveauJeu(Plateau plateau);
};

}

#endif // NIVEAUJEU_H
