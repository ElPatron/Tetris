#include <iostream>
#include <string>
#include "Joueur.h"
namespace tetris{

    Joueur::Joueur(std::string nom,bool multi): nomJoueur(nom),multiJoueur(multi),sacJoueur(){

    }

    Joueur::Joueur(Joueur const& jouerACopier):Joueur(jouerACopier.nomJoueur,jouerACopier.multiJoueur){

    }

    std::string Joueur::getNom(){
        return nomJoueur;
    }

   Sac Joueur::getSac(){
       return sacJoueur;
   }

   void Joueur::ajouterPiece(Brique &brique){
        sacJoueur.addBrique(brique);
   }

}
