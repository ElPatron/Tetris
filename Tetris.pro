TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Brique.cpp \
    Case.cpp \
    Carree.cpp \
    Joueur.cpp \
    NiveauJeu.cpp \
    point.cpp \
    brique.cpp

HEADERS += \
    Brique.h \
    Carree.h \
    Case.h \
    Joueur.h \
    Interaction.h \
    Move.h \
    NiveauJeu.h \
    Observable.h \
    Observer.h \
    Plateau.h \
    Point.h \
    Sac.h \
    Score.h \
    Online.h \
    timer.h
