#ifndef CARREE_H
#define CARREE_H
#include <iostream>
#include "Point.h"
/*!
 * \brief Espace de nom de tétris.
 *
 */
namespace tetris {
/*!
 * \brief Le carrée représente le petit carree qui permet de constituer une brique.
 */
class Carree:public Point{
public:
    /*!
     * \brief Carree Construit un nouveau carrée.
     * \param x Le x du carrée
     * \param y Le y du carrée
     */
    Carree(int x,int y);

    ~Carree();

    /*!
     * \brief carree Constructeur par recopie de carree
     * \param carree Le carree à recopier
     */
    Carree(Carree const& carree);
};
}
#endif // CARREE_H
